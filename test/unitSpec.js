/* jshint -W030 */
/* globals define, expect */
(function (factory) {
	'use strict';
    if (typeof define === 'function' && define.amd) {
        define('test/unitSpec', ['converter'], function(Converter){
            factory(expect, Converter);
        });
    } else if (typeof exports === 'object') {
        module.exports = factory(require('expect.js'), require('../lib/converter'));
    }
}(function (expect, Converter) {
	'use strict';

    describe('Unit', function(){

        var byte, kilobyte;

        beforeEach(function(){
            byte = Converter.Unit({
                name: 'bytes',
                abbr: 'B',
                code: 'B'
            });
            kilobyte = Converter.Unit({
                name: 'kilobytes',
                abbr: 'KB',
                code: 'KB',
                base: [1024, byte]
            });
        });

        describe('#getCode', function(){
            it('should return provided code', function(){
                expect(byte.getCode()).to.equal('B');
                expect(kilobyte.getCode()).to.equal('KB');
            });
        });

        describe('#getConversionRate', function(){
            it('should return base ratio which defaults to 1 for base units', function(){
                expect(byte.getConversionRate()).to.equal(1).and.be.a.Number;
            });
            it('should return valid base ratio of non-base unit', function(){
                expect(kilobyte.getConversionRate()).to.equal(1024).and.be.a.Number;
            });
        });

        describe('#extend ({String} name, {String} abbr, {String} code, {Number} ratio)', function(){
            it('should create new instance of unit with base equal to the creators base', function(){
                var megabyte = byte.extend('megabyte', 'MB', 'MB', 1024 * 1024);
                expect(megabyte).to.be.a(Converter.Unit);
                expect(megabyte.getConversionRate()).to.equal(1024 * 1024);
            });
        });

        describe('#add ({String} code, {String} name, {String} abbr, {Number} ratio)', function(){
            it('should add child unit to the base', function(){
                byte.add('KB', 'kilobyte', 'KB', 1024);
                expect(byte.get('KB')).to.be.a(Converter.Unit);
                expect(byte.get('KB').getCode()).to.equal('KB');
                expect(byte.get('KB').getBase()).to.equal(byte);
                expect(byte.get('KB').getConversionRate()).to.equal(1024);
            });
        });

    });

}));

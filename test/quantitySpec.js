/* jshint -W030 */
/* globals define, expect */
(function (factory) {
	'use strict';
    if (typeof define === 'function' && define.amd) {
        define('test/quantitySpec', ['converter'], function(Converter){
            factory(expect, Converter);
        });
    } else if (typeof exports === 'object') {
        module.exports = factory(require('expect.js'), require('../lib/converter'));
    }
}(function (expect, Converter) {
	'use strict';

    describe('Qunatity', function(){

        var gram = Converter.Unit({
            name: 'gram',
            abbr: 'g'
        });
        var pc = Converter.Unit({
            name: 'pieces',
            abbr: 'pc.'
        });
        var box10 = Converter.Unit({
            name: 'boxes',
            abbr: 'box',
            base: [10, pc]
        });
        var box20 = Converter.Unit({
            name: 'boxes',
            abbr: 'box',
            base: [20, pc]
        });
        var pc100 = Converter.Quantity(100, pc);

        describe('#convertTo ({Unit} unit)', function(){
            it('should convert unit and return new Quantity object', function(){
                expect(pc100.convertTo(box10)).to.be.a(Converter.Quantity);
                expect(pc100.convertTo(box20)).to.be.a(Converter.Quantity);
                expect(pc100.convertTo(box10).valueOf()).to.equal(10);
                expect(pc100.convertTo(box20).valueOf()).to.equal(5);
            });
            it('should throw error when unit base do not match or provided value is not an instance of Unit', function(){
                expect(function(){
                    pc100.convertTo(gram);
                }).to.throwError('Unit base missmatch');
                expect(function(){
                    pc100.convertTo(null);
                }).to.throwError('Argument should be instance of Unit');
            });
            it('should convert to base when no unit is provided', function(){
                expect(Converter.Quantity(50, box20).convertTo().toString()).to.equal('1000 pc.');
            });
        });

        describe('#substract ({Number|Quantity} quantity, [{Unit} unit])', function(){
            it('should substract one Qunatity from another', function(){
                expect(Converter.Quantity(50, pc).substract(Converter.Quantity(25, pc)).toString()).to.equal('25 pc.');
                expect(Converter.Quantity(50, pc).substract(25).toString()).to.equal('25 pc.');
                expect(Converter.Quantity(50, pc).substract('25').toString()).to.equal('25 pc.');
            });
            it('should substract one Qunatity from another but with different Unit', function(){
                expect(Converter.Quantity(50, pc).substract(Converter.Quantity(2, box10)).toString()).to.equal('30 pc.');
                expect(Converter.Quantity(50, pc).substract(2, box10).toString()).to.equal('30 pc.');
                expect(Converter.Quantity(50, pc).substract('2', box10).toString()).to.equal('30 pc.');
            });
        });

        describe('#add ({Number|Quantity} quantity, [{Unit} unit])', function(){
            it('should substract one Qunatity from another', function(){
                expect(Converter.Quantity(50, pc).add(Converter.Quantity(25, pc)).toString()).to.equal('75 pc.');
                expect(Converter.Quantity(50, pc).add(25).toString()).to.equal('75 pc.');
                expect(Converter.Quantity(50, pc).add('25').toString()).to.equal('75 pc.');
            });
            it('should substract one Qunatity from another but with different Unit', function(){
                expect(Converter.Quantity(50, pc).add(Converter.Quantity(2, box10)).toString()).to.equal('70 pc.');
                expect(Converter.Quantity(50, pc).add(2, box10).toString()).to.equal('70 pc.');
                expect(Converter.Quantity(50, pc).add('2', box10).toString()).to.equal('70 pc.');
            });
        });

    });

}));

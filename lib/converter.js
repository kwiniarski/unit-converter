/* globals define */
(function (factory) {
	'use strict';
    if (typeof define === 'function' && define.amd) {
        define('converter', ['quantity', 'unit'], factory);
    } else if (typeof exports === 'object') {
        module.exports = factory(require('./quantity'), require('./unit'));
    }
}(function (Quantity, Unit) {
	'use strict';

    function Converter() {
        this.units = {};
    }
    
    Converter.Quantity = Quantity;
    Converter.Unit = Unit;

    Converter.prototype.addUnit = function (unit) {
        this.units[unit.getCode()] = unit;
    };

    Converter.prototype.find = function (unit) {
        if (!this.units[unit]) {
            throw new Error('Unit is not registered');
        }
        return this.units[unit];
    };

    Converter.prototype.exchange = function (amount, ofUnit, toUnit) {

        if (typeof toUnit === 'string') {
            toUnit = this.find(toUnit);
        }

        if (typeof ofUnit === 'string') {
            ofUnit = this.find(ofUnit);
        }

        return new Quantity(amount, ofUnit).convertTo(toUnit || ofUnit.getBase());
    };

    Converter.prototype.move = function (amount, ofQuantity, toQuantity) {
        return;
    };

    return Converter;

}));

/* jshint -W030 */
/* globals define, expect */
(function (factory) {
	'use strict';
    if (typeof define === 'function' && define.amd) {
        define('test/converterSpec', ['converter'], function(Converter){
            factory(expect, Converter);
        });
    } else if (typeof exports === 'object') {
        module.exports = factory(require('expect.js'), require('../lib/converter'));
    }
}(function (expect, Converter) {
	'use strict';

    describe('Converter', function(){

        var conv,
            bytes,
            kiloBytes,
            megaBytes,
            gram;

        beforeEach(function(){
            conv = new Converter();

            bytes = Converter.Unit({
                'name': 'bytes',
                'abbr': 'B',
                'code': 'B'
            });
            megaBytes = Converter.Unit({
                'name': 'megabytes',
                'abbr': 'MB',
                'code': 'MB',
                'base': [1024 * 1024, bytes]
            });
            kiloBytes = Converter.Unit({
                'name': 'kilobytes',
                'abbr': 'KB',
                'code': 'KB',
                'base': [1024, bytes]
            });
            gram = Converter.Unit({
                'name': 'gram',
                'abbr': 'g',
                'code': 'g'
            });

            conv.addUnit(bytes);
            conv.addUnit(megaBytes);
            conv.addUnit(kiloBytes);
            conv.addUnit(gram);
        });

        describe('#exchange ({Number} qunatity, {Unit} from, [{Unit}, to])', function(){
            it('should fail when units have different base', function(){
                expect(function(){
                    conv.exchange(1024, bytes, gram);
                }).to.throwError('Unit base missmatch');
            });
            it('should return Converter.Quantity instance', function(){
                expect(conv.exchange(1, kiloBytes)).to.be.a(Converter.Quantity);
            });
            it('should convert one unit to another', function(){
                expect(Number(conv.exchange(1024, kiloBytes, megaBytes))).to.equal(1);
                expect(String(conv.exchange(1024, kiloBytes, megaBytes))).to.equal('1 MB');
            });
            it('should convert to base unit when conversion target is not specified', function(){
                expect(Number(conv.exchange(1, kiloBytes))).to.equal(1024);
                expect(String(conv.exchange(1, kiloBytes))).to.equal('1024 B');
            });
            it('should be able to use code as 2nd and 3rd argument', function(){
                expect(Number(conv.exchange(1024, 'KB', 'MB'))).to.equal(1);
                expect(Number(conv.exchange(1, 'KB'))).to.equal(1024);
            });
            it('should throw error when code is not recognized', function(){
                expect(function(){
                    conv.exchange(1, 'KG');
                }).to.throwError('Unit is not registered');
            });
        });

    });

}));

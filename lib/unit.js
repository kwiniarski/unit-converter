/* globals define */
(function (factory) {
	'use strict';
    if (typeof define === 'function' && define.amd) {
        define('unit', [], factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    }
}(function () {
	'use strict';

    function Unit(options) {
        if (this instanceof Unit === false) {
            return new Unit(options);
        }

        this.code = options.code;
        this.name = options.name;
        this.abbr = options.abbr;
        this.base = options.base || [1, this];

        this.family = {};
    }

    Unit.prototype.getName = function () {
        return this.name;
    };

    Unit.prototype.getCode = function () {
        return this.code;
    };

    Unit.prototype.getBase = function () {
        return this.base[1];
    };

    Unit.prototype.getConversionRate = function () {
        return Number(this.base[0]);
    };

    Unit.prototype.format = function (qty) {
        return qty + ' ' + this.abbr;
    };

    Unit.prototype.hasSameBaseAs = function (unit) {
        return this.getBase() === unit.getBase();
    };

    Unit.prototype.extend = function (name, abbr, code, ratio) {
        return new Unit({
            name: name,
            abbr: abbr,
            code: code,
            base: [ratio, this.getBase()]
        });
    };

    Unit.prototype.add = function (code) {
        this.family[code] = this.extend.apply(this, arguments);
    };

    Unit.prototype.get = function (code) {
        return this.family[code];
    };

    Unit.prototype.valueOf = function () {
        return {
            code: this.code,
            name: this.name,
            abbr: this.abbr,
            base: this.base
        };
    };

    return Unit;

}));

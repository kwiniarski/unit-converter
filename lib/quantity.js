/* globals define */
(function (factory) {
	'use strict';
    if (typeof define === 'function' && define.amd) {
        define('quantity', ['unit'], factory);
    } else if (typeof exports === 'object') {
        module.exports = factory(require('./unit'));
    }
}(function (Unit) {
	'use strict';

    function Quantity(qty, unit) {
        if (this instanceof Quantity === false) {
            return new Quantity(qty, unit);
        }

        this.qty = Number(qty);
        this.unit = unit;
    }

    Quantity.prototype.valueOf = function (unit) {
        return (unit instanceof Unit) ? this.convertTo(unit).valueOf() : this.qty;
    };

    Quantity.prototype.toString = function () {
        return this.unit.format(this.qty);
    };

    Quantity.prototype.convertTo = function (unit) {
        if (unit === undefined) {
            unit = this.unit.getBase();
        }
        if (false === unit instanceof Unit) {
            throw new Error('Argument should be instance of Unit');
        }
        if (false === unit.hasSameBaseAs(this.unit)) {
            throw new Error('Unit base missmatch');
        }
        return new Quantity(this.qty * this.unit.getConversionRate() / unit.getConversionRate(), unit);
    };

    Quantity.prototype.create = function (quantity, unit) {
        if (quantity instanceof Quantity) {
            return quantity;
        }
        if (typeof quantity === "string") {
            quantity = Number(quantity);
        }
        if (typeof quantity === "number") {
            return new Quantity(quantity, (unit instanceof Unit) ? unit : this.unit);
        }
    };

    Quantity.prototype.substract = function (quantity, unit) {
        return new Quantity(this.qty - this.create(quantity, unit).valueOf(this.unit), this.unit);
    };

    Quantity.prototype.add = function (quantity, unit) {
        return new Quantity(this.qty + this.create(quantity, unit).valueOf(this.unit), this.unit);
    };

    return Quantity;

}));
